package com.corelogic.propertymanager.utilities;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(PropertyNotFoundException.class)
    @ResponseBody
    public ResponseEntity<JsonResponseBody> updatePropertyExceptionHandler (PropertyNotFoundException pnfe, HttpServletRequest httpServletRequest) {

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new JsonResponseBody(HttpStatus.NOT_FOUND.value(), "Property not found " + pnfe.toString()));
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseBody
    public ResponseEntity<JsonResponseBody> badRequestExceptionHandler (BadRequestException e) {

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new JsonResponseBody(HttpStatus.BAD_REQUEST.value(), "Data not valid: " + e.toString()));
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<JsonResponseBody> exceptionHandler (Exception e) {

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new JsonResponseBody(HttpStatus.INTERNAL_SERVER_ERROR.value(), "There was an internal error " + e.toString()));
    }
 }
