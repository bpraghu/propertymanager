package com.corelogic.propertymanager.controller;

import com.corelogic.propertymanager.entities.Property;
import com.corelogic.propertymanager.service.PropertyService;
import com.corelogic.propertymanager.utilities.BadRequestException;
import com.corelogic.propertymanager.utilities.NoContentFoundException;
import com.corelogic.propertymanager.utilities.PropertyNotFoundException;
import com.corelogic.propertymanager.utilities.PropertyValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * This class exposes the end points of the PropertyManager application.
 */
@RestController
public class PropertyController {

    @Autowired
    private PropertyService propertyService;

    @Value("${propertymanager.pagination.size}")
    private int paginationSize;

    /**
     * Shows all the stored properties in the DB
     *
     * @param pageNum the Page number
     * @return list of Properties
     */
    @RequestMapping("/showProperties")
    @ResponseBody
    public Page<Property> showProperties(@RequestParam("pageNum") int pageNum) throws NoContentFoundException {

        Page<Property> propertiesPage = propertyService.getProperties(pageNum, this.paginationSize);
        if(propertiesPage.getTotalElements() == 0)
            throw new NoContentFoundException("No data found");

        return propertiesPage;
    }

    /**
     * Method to add a new property
     *
     * @param property the <code>Property</code> to be added
     * @param result <code>BindingResult</code> object
     * @return the newly added Property
     */
    @RequestMapping(value="/newProperty", method=POST)
    @ResponseBody
    public Property addNewProperty(@RequestBody Property property, BindingResult result) throws BadRequestException {

        // used to validate the property data
        PropertyValidator validator = new PropertyValidator();
        validator.validate(property, result);

        // Check for errors and return an appropriate response
        if(result.hasErrors()){
            throw new BadRequestException("Invalid input data");
        }

        Property newPropertyResource = propertyService.addProperty(property);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(newPropertyResource.getPropertyId()).toUri();
        return newPropertyResource;
    }

    /**
     * Method to update a <code>Property</code>
     *
     * @param propertyId
     * @param property
     * @param result
     * @return the updated Property
     */
    @RequestMapping(value="/updateProperty/{propertyId}", method=PUT)
    public Property updateProperty(@PathVariable("propertyId") long propertyId, @RequestBody Property property, BindingResult result) throws BadRequestException, PropertyNotFoundException {

        // used to validate the property data
        PropertyValidator validator = new PropertyValidator();
        validator.validate(property, result);

        // Check for errors
        if(result.hasErrors()){
            throw new BadRequestException("Invalid input data");
        }

        return propertyService.updateProperty(property, propertyId);
    }
}
